<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Department */

$this->title = 'สร้างข้อมูล';
$this->params['breadcrumbs'][] = ['label' => 'Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('../admin/_mainmenu') ?>

<?= $this->render('../_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="department-create">
    <h5 style="font-weight: bold;"><?= $this->title ?></h5>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
