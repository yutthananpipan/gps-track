<?php

/* @var $this yii\web\View */

$this->title = 'DRC GPS-TRACK';

use yii\helpers\Url; ?>


<div class="contain-side">
    <div id="device-menu" class="esri-widget show">
        <div class="device-menu">
            <div class="text-center mt-4">
                <img src="<?= Url::to('@web/img/LOGO-DRC-Innovation.png') ?>" alt="DRC Logo" class="img"
                     style="opacity: .8; height: 40px; width: auto;">
            </div>
            <hr/>
            <h4 id="head" class="pl-4 ml-2 text-light"> GPS Tracking</h4>

            <div id="nyc_graphics"></div>

        </div>
    </div>

    <div id="map"></div>

</div>
<link rel="stylesheet" href="https://js.arcgis.com/4.17/esri/themes/dark/main.css"/>
<script src="https://js.arcgis.com/4.17/"></script>
<script type="text/javascript" src="<?= Url::to('@web/js/main.js') ?>"></script>
