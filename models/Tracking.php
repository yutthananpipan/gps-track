<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%tracking}}".
 *
 * @property int $id
 * @property string $device_name Device UUID
 * @property float|null $Lat
 * @property float|null $Lng
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property Device $deviceName
 */
class Tracking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tracking}}';
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Lat', 'Lng'], 'number'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['device_name'], 'string', 'max' => 255],
            [['device_name', 'Lat', 'Lng'], 'unique', 'targetAttribute' => ['device_name', 'Lat', 'Lng']],
            [['device_name'], 'exist', 'skipOnError' => true, 'targetClass' => Device::className(), 'targetAttribute' => ['device_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_name' => 'Device Name',
            'Lat' => 'Lat',
            'Lng' => 'Lng',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[DeviceName]].
     *
     * @return \yii\db\ActiveQuery|DeviceQuery
     */
    public function getDeviceName()
    {
        return $this->hasOne(Device::className(), ['name' => 'device_name']);
    }

    /**
     * {@inheritdoc}
     * @return TrackingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrackingQuery(get_called_class());
    }
}
