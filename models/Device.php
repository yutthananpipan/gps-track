<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%device}}".
 *
 * @property string $name
 * @property int $status_id
 * @property int $update_interval ช่วงเวลาดึงข้อมูล (second)
 * @property float|null $Lat Latitude 
 * @property float|null $Lng Longitude
 * @property int $monitor 0 : do not monitor, 1: monitor 
 * @property int|null $created_at เวลาที่สร้างข้อมูล
 * @property int|null $created_by สร้างข้อมูลโดย
 * @property int|null $updated_at เวลาที่อัพเดทข้อมูล
 * @property int|null $updated_by อัพเดทข้อมูลโดย
 * @property int|null $last_on เวลาที่ใช้งานล่าสุด
 * @property int|null $update_request
 *
 * @property Status $status
 * @property User $createdBy
 * @property User $updatedBy
 * @property Tracking[] $trackings
 */
class Device extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%device}}';
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status_id', 'update_interval', 'monitor', 'created_at', 'created_by', 'updated_at', 'updated_by', 'last_on', 'update_request'], 'integer'],
            [['Lat', 'Lng'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'status_id' => 'Status ID',
            'update_interval' => 'Update Interval',
            'Lat' => 'Lat',
            'Lng' => 'Lng',
            'monitor' => 'Monitor',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'last_on' => 'Last On',
            'update_request' => 'Update Request',
        ];
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery|StatusQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[Trackings]].
     *
     * @return \yii\db\ActiveQuery|TrackingQuery
     */
    public function getTrackings()
    {
        return $this->hasMany(Tracking::className(), ['device_name' => 'name']);
    }

    /**
     * {@inheritdoc}
     * @return DeviceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DeviceQuery(get_called_class());
    }
}
