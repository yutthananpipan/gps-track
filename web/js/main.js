require([
    "esri/Graphic",
    "esri/views/SceneView",
    "esri/WebScene",
    "esri/symbols/PictureMarkerSymbol",
    "esri/symbols/TextSymbol",
    "esri/widgets/Expand"
], function (Graphic, SceneView, WebScene, PictureMarkerSymbol, TextSymbol, Expand) {

    const state = {
        offline: '0',
        online: '1',

    };

    class Device {
        constructor(name, last_on, state, IconSymbol, TextSymbol) {
            this.name = name;
            this.last_on = last_on;
            this.state = state;
            this.IconSymbol = IconSymbol;
            this.TextSymbol = TextSymbol;
        }
    }

    var socket_host = 'http://171.99.188.118';
    var socket_port = 8086;
    var socket;
    var username = uuidv4();
    var rows = [];

    var scene = new WebScene({
        basemap: "hybrid",
        ground: "world-elevation"

    });
    var view = new SceneView({
        container: "map",
        map: scene,
        padding: {
            top: 0,
            right: 0,
        },
        scale: 500000,
        center: [100.575236, 13.785954],
        ui: {
            components: ["attribution"] // replace default set of UI components
        }
    });

    view.ui.add(
        new Expand({
            view: view,
            content: document.getElementById("device-menu"),
            expandIconClass: "esri-icon-layer-list",
            expanded: true
        }),
        "top-right"
    );

    loadDeviceList();
    IntervalDeviceData();

    function loadDeviceList() {
        let path = location.origin + location.pathname;
        $.get(path + '/device/get-devices', function (res) {
            console.log(res.toString());
            Object.keys(res).forEach(function (index) {
                console.log(res[index].name);
                let name = res[index].name;
                let latitude = (res[index].lat === null) ? 0 : res[index].lat;
                let longitude = (res[index].lng === null) ? 0 : res[index].lng;
                let last_on = (res[index].last_on === null) ? 0 : res[index].last_on;

                let jsObj = JSON.stringify({
                    "name": name,
                    "last_on": last_on,
                    "lat": latitude,
                    "lng": longitude,
                    "state": state.offline
                });

                UpdateRows(jsObj);
            });

            UpdateViews();
            SocketIOConnection(socket_host, socket_port);

        }, 'json').fail(function (e) {
        });
    }

    function SocketIOConnection(host, port) {
        try {
            socket = io.connect(host + ':' + port, {
                reconnection: true,
                reconnectionDelay: 1000,
                reconnectionDelayMax: 5000,
                reconnectionAttempts: Infinity
            });

            // Event handler for new connections.
            // The callback function is invoked when a connection with the
            // server is established.
            socket.on('connect', function () {
                console.log('connected');
                socket.emit('state', username);

            });

            // Event handler for server sent data.
            // The callback function is invoked whenever the server emits data
            // to the client. The data is then displayed in the "Received"
            // section of the page.
            socket.on('new message', function (msg) {
                console.log(msg.message);
                UpdateRows(msg.message.toString());
                UpdateViews();

            });

            // Whenever the server emits 'login', log the login message
            socket.on('login', function (data) {
                // Display the welcome message
                var message = "Welcome to Socket.IO Chat – ";
                console.log(message);

            });

            // Whenever the server emits 'user joined', log it in the chat body
            socket.on('user joined', function (data) {
                console.log(data.username + ' joined');

                try {
                    let device = getDevice(data.username);

                    if (device != null) {
                        let jsObj = JSON.stringify(getData(device, state.online));

                        UpdateRows(jsObj);
                        UpdateViews();
                    }
                } catch (e) {

                }
            });

            // Whenever the server emits 'user left', log it in the chat body
            socket.on('user left', function (data) {
                console.log(data.username + ' left');
                try {
                    let device = getDevice(data.username);

                    if (device != null) {
                        let jsObj = JSON.stringify(getData(device, state.offline));

                        UpdateRows(jsObj);
                        UpdateViews();
                    }
                } catch (e) {

                }

            });

            socket.on('disconnect', function () {
                console.log('you have been disconnected');
            });

            socket.on('reconnect', function () {
                console.log('you have been reconnected');
                if (username) {
                    socket.emit('add user', username);
                }
            });

            socket.on('reconnect_error', function () {
                console.log('attempt to reconnect has failed');
            });

            socket.emit('add user', username);
        } catch (e) {
            console.log(e.toString());
        }
    }

    function getData(device, st) {
        return {
            "name": device.name,
            "last_on": device.last_on,
            "lat": device.IconSymbol.geometry.latitude,
            "lng": device.IconSymbol.geometry.longitude,
            "state": st
        }
    }

    function UpdateRows(dataIn) {
        try {
            let obj = JSON.parse(dataIn);
            let name = obj.name.toString();
            let last_on = obj.last_on.toString();
            let lat = obj.lat.toString();
            let lng = obj.lng.toString();
            let state = obj.state.toString();

            let point = {
                type: "point", // autocasts as new Point()
                longitude: lng,
                latitude: lat
            };

            if (name.trim() !== "") {
                let count = 0;
                let symbol = createSymbol(point, state);
                let textSymbol = createTextSymbol(point, name);

                let device = new Device(name, last_on, state, symbol, textSymbol);
                for (let i = 0; i < rows.length; i++) {
                    if (device.name === rows[i].name) {
                        rows.splice(i, 1, device);
                        count++;

                    }
                }

                if (count === 0) {
                    rows.push(device);
                }

                //sort
                rows.sort((b, a) => (a.last_on > b.last_on) ? 1 : ((b.last_on > a.last_on) ? -1 : 0));

            }
        } catch (e) {

        }
    }

    function UpdateViews() {
        try {
            // Todo button List of Device
            const rootNode = document.getElementById("nyc_graphics");
            const fragment = document.createDocumentFragment();

            for (let index = 0; index < rows.length; index++) {
                const device = rows[index];
                const name = device.name;
                const status = device.state;

                // Create a list zip codes in NY
                const li = document.createElement("li");
                li.classList.add("panel-result");
                li.tabIndex = 0;
                li.setAttribute("data-result-id", index.toString());
                li.setAttribute("id", name);

                // Todo create Icon
                const tagicon = document.createElement("i");
                tagicon.setAttribute("id", "icon" + index.toString());
                tagicon.classList.add("fa");
                tagicon.classList.add("fa-circle");
                tagicon.setAttribute("id", name);
                tagicon.style.color = (status === state.online) ? "green" : "orange";
                li.appendChild(tagicon);

                // Todo create Device Name
                const tagname = document.createElement("span");
                tagname.style.marginLeft = "2px";
                tagname.innerHTML = name;
                tagname.setAttribute("id", name);
                li.appendChild(tagname);

                // element for device list
                fragment.appendChild(li);
            }

            rootNode.innerHTML = "";
            rootNode.appendChild(fragment);

            // listen to click event on the zip code list
            rootNode.addEventListener("click", onListClickHandler);

            UpdateStatusDeviceOnmap();
        } catch (e) {

        }
    }

    function onListClickHandler(event) {
        let target = event.target;
        let resultId = target.getAttribute("id");
        console.log(resultId);

        try {
            let device = getDevice(resultId);
            if (device != null) {

                view.center = {
                    type: "point",
                    longitude: device.IconSymbol.geometry.longitude,
                    latitude: device.IconSymbol.geometry.latitude,
                };
                view.scale = 5000;
                view.tilt = 50;
            }
        } catch (e) {

        }
    }

    function UpdateStatusDeviceOnmap() {
        view.graphics.removeAll();
        for (let i = 0; i < rows.length; i++) {
            let device = rows[i];
            // add symbol
            view.graphics.add(device.IconSymbol);
            view.graphics.add(device.TextSymbol);
        }
    }

    function getDevice(name) {
        try {
            for (let i = 0; i < rows.length; i++) {
                if (name === rows[i].name) {
                    return rows[i];

                }
            }
        } catch (e) {

        }

        return null;
    }

    function createSymbol(point, st) {
        if (st === state.online) {
            return new Graphic({
                geometry: point,
                symbol: {
                    type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
                    color: [0, 200, 0],
                    outline: {
                        // autocasts as new SimpleLineSymbol()
                        color: [255, 255, 255],
                        width: 2
                    }
                }

            });
        } else {
            return new Graphic({
                geometry: point,
                symbol: {
                    type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
                    color: [128, 128, 128],
                    outline: {
                        // autocasts as new SimpleLineSymbol()
                        color: [255, 255, 255],
                        width: 2
                    }
                }

            });
        }
    }

    function createTextSymbol(point, text) {
        return new Graphic({
            geometry: point,
            symbol: {
                type: "text",  // autocasts as new TextSymbol()
                text: "             " + text,
                color: "white",
                haloColor: "black",
                haloSize: "1px",
                xoffset: 30,
                yoffset: 30,
                verticalAlignment: "top",
                horizontalAlignment: "right",
                lineHeight: 6,
                font: {  // autocasts as new Font()
                    size: 8,
                    family: "Josefin Slab",
                    weight: "normal"
                }

            }
        });
    }

    function uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    function IntervalDeviceData() {
        setInterval(function () {
            try {
                console.log(new Date());
                const newrow = rows;
                for (let i = 0; i < newrow.length; i++) {
                    let name = newrow[i].name;
                    try {
                        let device = getDevice(name);

                        if (device != null) {
                            let jsObj = JSON.stringify(getData(device, state.offline));

                            UpdateRows(jsObj);

                        }
                    } catch (e) {

                    }
                }

                UpdateViews();
                socket.emit('state', username);
            } catch (e) {

            }
        }, 60 * 1000);
    }

});






