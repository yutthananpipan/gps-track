#!/usr/bin/env python
import time
import serial


# convert string to hex
def toHex(s):
    lst = []
    lst.append(int('0x02', 16))
    for ch in s:
        hv = hex(ord(ch)).replace('0x', '')
        if len(hv) == 1:
            hv = '0' + hv
        lst.append(int('0x' + hv, 16))

    lst.append(int('0x03', 16))
    # return reduce(lambda x, y: x + y, lst)
    return lst


def WriteSerial(cmd):
    try:
        #ser.write(serial.to_bytes(toHex(cmd)))
        ser.write(cmd)
        time.sleep(1)

        return 'True'
    except:
        print('error: cmd = ' + cmd)
        return 'False'


if __name__ == "__main__":
    ser = serial.Serial(
        port='/dev/ttyS0',
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
    )

    WriteSerial('test')
