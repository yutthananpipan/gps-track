from socketIO_client import SocketIO, LoggingNamespace
import time
import json
import GPS
from User import User

host = 'http://171.99.188.118'
socket_port = 8086
http_port = 8085
socketIO = None
gps = None


def on_connect():
    try:
        print('connect')
        if socketIO is not None:
            socketIO.emit('new message', json.dumps(gps.getData()))
    except:
        print("error")


def on_login():
    try:
        print('Welcome to socket io')
        if socketIO is not None:
            socketIO.emit('new message', json.dumps(gps.getData()))
    except:
        print("error")


def on_disconnect():
    print('disconnect')
    time.sleep(5)
    socketIO.emit("add user", user.username)


def on_reconnect():
    print('reconnect')
    time.sleep(5)
    socketIO.emit("add user", user.username)


def on_joinuser(*args):
    print('on_joinuser', args)


def on_state(*args):
    try:
        print('on_state', args)
        if socketIO is not None:
            socketIO.emit('new message', json.dumps(gps.getData()))
    except:
        print("error")


def on_massage(*args):
    try:
        print('on_newmassage', args)
    except:
        print("Convert Error")


def SocketIOConnect():
    global socketIO, gps
    try:
        socketIO = SocketIO(host=host, port=socket_port)
        socketIO.on('connect', on_connect)
        socketIO.on('disconnect', on_disconnect)
        socketIO.on('reconnect', on_reconnect)

        # Listen
        socketIO.on('new message', on_massage)
        socketIO.on('user joined', on_joinuser)
        socketIO.on('state', on_state)

        socketIO.emit('add user', user.username)

        gps = GPS.instance(socketio=socketIO, host=host, port=http_port)

        socketIO.wait()

    except:
        print("SocketIO Exception")
        time.sleep(5)
        SocketIOConnect()


if __name__ == "__main__":
    user = User()
    print(user.username + ' running')
    SocketIOConnect()
