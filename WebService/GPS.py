from math import sin, cos, sqrt, atan2, radians
import json
from threading import Thread
import requests
import time
from User import User
import serial

# Todo Init Location
lat_new = 0
lng_new = 0
lat_old = 0
lng_old = 0
# Todo End #########

user = User()
gps = None
socketIO = None
http_host = None
http_port = None


class MainThread(Thread):
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        global gps, socketIO, lat_new, lng_new, lat_old, lng_old
        while True:
            time.sleep(10)
            try:
                if socketIO is not None:
                    data = gps.getData()
                    socketIO.emit('new message', json.dumps(data))

                    distance = GetDistance(lat_old, lng_old, lat_new, lng_new)

                    if distance >= 20:
                        # http request send
                        r = requests.get(
                            http_host + ':' + str(http_port) + '/gps-track/web/device/update-device?data='
                            + str(json.dumps(data))
                        )

                        lat_old = lat_new
                        lng_old = lng_new

                        print('distance = ' + str(distance))

                    print('gps sending...')

            except:
                print("error")


class GPSService(Thread):
    def __init__(self):
        Thread.__init__(self)
        try:
            self.ser = serial.Serial(
                port='/dev/ttyS0',
                baudrate=4800,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1
            )
        except:
            print('Init Serial Error')

    def run(self):
        global lat_new, lng_new

        while True:
            r = self.ser.readline()
            try:
                r = str(r).split(",")
                if "GPGGA" in r[0]:
                    if str(r[2]).strip() not in '' and str(r[4]).strip() not in '':
                        lat_new = self.DDM2DD(r[2])
                        lng_new = self.DDM2DD(r[4])
                        print(lat_new, lng_new)

            except:
                print("error")

    def DDM2DD(self, num):
        try:
            inputDegrees = int(float(num) / 100)
            inputMinutes = (num.split(".")[0]).split(str(inputDegrees))[1] + '.' + num.split(".")[1]
            value = inputDegrees + (float(inputMinutes) / 60)
            return value

        except:
            print('error: src = ' + num)
            return 0


class GPS:
    def __init__(self):
        print('GPS Init')

        gpsService = GPSService()
        gpsService.daemon = True

        mainThread = MainThread()
        mainThread.daemon = True

        gpsService.start()
        mainThread.start()

    def getData(self):
        latitude, longitude = lat_new, lng_new
        data = {
            "name": user.username,
            "last_on": user.GetTime(),
            "state": 1,
            "lat": latitude,
            "lng": longitude
        }

        return data


def GetDistance(lat1, lng1, lat2, lng2):
    # approximate radius of earth in km
    R = 6373.0
    lon1, lat1, lon2, lat2 = map(radians, [lng1, lat1, lng2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    print("Result:", distance)

    return distance * 1000


def instance(socketio, host, port):
    global gps, socketIO, http_host, http_port
    gps = GPS()
    socketIO = socketio
    http_host = host
    http_port = port
    return gps
