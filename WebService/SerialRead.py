#!/usr/bin/env python
import time
import serial

ser = serial.Serial(
    port='com13',
    baudrate=4800,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
)

while 1:
    r = ser.readline()
    try:
        lat_new, lng_new = 0, 0
        r = str(r).split(",")
        if "GPGGA" in r[0]:
            if str(r[2]).strip() not in '' and str(r[4]).strip() not in '':
                lat_new = float(r[2]) / 100
                lng_new = float(r[4]) / 100

    except:
        print("Exception")
