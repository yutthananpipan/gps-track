<?php

namespace app\controllers;

use app\models\Tracking;
use phpDocumentor\Reflection\Types\Object_;
use Yii;
use app\models\Device;
use app\models\DeviceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeviceController implements the CRUD actions for Device model.
 */
class DeviceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Device models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeviceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Device model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Device model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Device();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Device model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Device model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Device model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Device the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Device::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetDevices()
    {
        $models = Device::find()->orderBy(['last_on' => SORT_DESC])->limit(400)->all();

        $index = 0;
        $json_arrays = array();
        foreach ($models as $m) {
            $obj = new Object_();
            $obj->name = $m->name;
            $obj->lat = $m->Lat;
            $obj->lng = $m->Lng;
            $obj->last_on = $m->last_on;
            $json_arrays[$index] = $obj;
            $index++;
        }

        //var_dump(Device::find()->select(['name'])->all());

        return json_encode($json_arrays);
    }

    public function actionUpdateDevice($data)
    {
        try {
            $obj = json_decode($data);
            $model = $this->findModel($obj->name);
            if ($model) {
                $model->last_on = $obj->last_on;
                $model->Lat = $obj->lat;
                $model->Lng = $obj->lng;
                $model->save();

                $tracking = new Tracking();
                $tracking->device_name = $model->name;
                $tracking->Lat = $model->Lat;
                $tracking->Lng = $model->Lng;
                $tracking->save();

            }
        } catch (\Exception $e) {
        }
    }
}
